

#include <Wire.h>
#include "elapsedMillis.h"
#include "SparkFunHTU21D.h"
#include "SparkFunMPL3115A2.h"
#include <MsTimer2.h>
#include <SoftwareSerial.h>

/* CONEXION */
//const String IP = "http://apilink.net";
const String IP = "apilink.net"; //mac (mirar en preferencias -> red)
const String RECURSO = "aupaMeteo";
const String PUERTO="8001";

//const String SSID="apilink";
//const String PASSWORD="apilink.net";
const String SSID="JAZZTEL-5WJC8D";
const String PASSWORD="KLEDNMR5";
/*
  cada pocos segundos salida por pantalla para depurar
  cada hora vuelca todo en el servidor (y resetea la racha y precipitacion)
*/

int MINUTOS = 1;
#define esp8266 Serial1
#define CADA_POCO 10000 //en milis (10 sg)

float HORA = MINUTOS*60;//
float CASI_HORA = (MINUTOS*60)-10;// 10 segundos antes
float cacito =0.5/174; //litros medido. aprox 0.0028 L
float AREA_PLU=5*11; //en centimetros
float CORRIGE_PLU=10000/AREA_PLU;
bool DEBUG = false;
bool MODO_KK = false;
bool LEVANTAR_AP = false;
int min = 0; // contador de minutos

//plu
const byte RAIN = 2;
volatile int cacitos = 0;
volatile int cambio=0;

//anemo
const byte ANEMO = 3;
volatile int anemoTics = 0;
volatile int anemoCambio = 0;

// PINES
const byte REFERENCE_3V3 = A3; //?
const byte LIGHT = A1;
const byte ESP_ENABLE = 51;
const byte WDIR = A0;

elapsedMillis esperaPoco, espera1h, esperaCasiHora, tViento, tRacha, lluviaPing;
HTU21D sensorTH;
MPL3115A2 sensorPresion;

// lecturas

float temp=0, humedad=0, litros=0, presion=0;
float rachaViento=0, direccionViento=0, luz=0;
volatile long pingsViento=0, pingsRacha; // clicks del anemometro, se resetean cad 5s
float vientoMedio=0;

// cabeceras de funciones
void anemoIRQ();
void rainIRQ();
void kk();
void hacerCadaPoco();
void hacerCadaHora();
void hacerAntesHora();
void leerLuz();
void leerTH();
void leerPresion();
void calcularRachaViento();
void resetEsp();
float get_light_level();
int leerVeleta();
bool leeEsp(int );
void resetear();
bool subir();
//---------------------------------------------------------------------------

  void eco(int s){

    int bucles=s*1000/10;
    char x=0;
    Serial.println(bucles);
    Serial.println("\n<Eco>\nW.connect('JAZZTEL-5WJC8D', 'KLEDNMR5')");
    Serial.println("\n<Eco>\nW.connect('apilink', 'apilink.net')");
    Serial.println("\nisok()");

    while (bucles>0){

        delay(10);
        bucles=bucles-1;
        //renvía del serie al esp para mandar comandos AT directamente

          while(Serial.available()>0){
                x=Serial.read();
                esp8266.write(x);
                bucles=s*1000/10; //vuelve a empezar

            }

            while(esp8266.available()>0)  Serial.print((char)esp8266.read());

    }
     Serial.println("\n</Eco>\n");

}

//---------------------------------------------------------------------------
void setup(){

  delay(2000);
  //esp8266.begin(115400);
  Serial.begin(115200);

  Serial.println("HORA y CASI_HORA: ");
  HORA = HORA*1000;
  CASI_HORA = CASI_HORA*1000; // 1 segndo antes de la hora
  Serial.println(HORA);
  Serial.println(CASI_HORA);

  //inicializar sensores -------------------------------------------------------
  sensorTH.begin();
  sensorPresion.begin();
  sensorPresion.setModeBarometer(); // Measure pressure in Pascals from 20 to 110 kPa
  sensorPresion.setOversampleRate(100); // Set Oversample to the ridmmended 128
  sensorPresion.enableEventFlags(); //

  //pluviometro
  pinMode(RAIN, INPUT_PULLUP); // input from wind meters rain gauge sensor
  attachInterrupt(digitalPinToInterrupt(RAIN), rainIRQ, FALLING);

  //anemometro
  pinMode(ANEMO, INPUT_PULLUP); // input from wind meters rain gauge sensor
  attachInterrupt(digitalPinToInterrupt(ANEMO), anemoIRQ, CHANGE);

  //luz
  pinMode(LIGHT, INPUT);

  //--- Configurar el ESP ------------------------------------------------------
  //digitalWrite(41, HIGH);
  /*
  pinMode(ESP_ENABLE, OUTPUT);
  pinMode(49, OUTPUT); //reset

  digitalWrite(49, HIGH);
  delay(1000);
  //digitalWrite(ESP_ENABLE, LOW);
  delay(1000);
  digitalWrite(ESP_ENABLE, HIGH);
  delay(1000);
  String con="\nW.connect('"+SSID+"', '"+PASSWORD+"')";
  esp8266.println(con);
  Serial.println(con);
  delay(5000);
  esp8266.println("\nisok()");
  leeEsp(5);


  if(LEVANTAR_AP){
    Serial.println("levantarAp()");
    esp8266.println("levantarAp()");
    leeEsp(3);
  }
  */

  //--- Varios -------
  if (MODO_KK) kk();

      //inicializa los timers
      esperaPoco=0;
      espera1h=0;
      tViento=0;
      cacitos=0;


}

//------------------------------------------------------------------------------
void kk(){

  hacerCadaPoco();
  hacerCadaHora();

}
//------------------------------------------------------------------------------
void loop(){

    if (esperaPoco>CADA_POCO){ hacerCadaPoco(); }
    if (esperaCasiHora>CASI_HORA){ hacerAntesHora(); }
    if (espera1h>HORA){ hacerCadaHora(); }

    // check pluviometro
    if (cambio==1){
        Serial.println(cacitos);
        cambio=0;
    }
    // check anemometro
    if (anemoCambio==1){
        Serial.print(".");
        anemoCambio=0;
    }

    /*
    //vaciar esp
    while(esp8266.available()>0) {

        char x=(char)esp8266.read();
        Serial.print(x);

    }
    */
}
/*************************************************
      F U N C I O N E S   D E l   T I E M P O
**************************************************/

void hacerCadaPoco(){

    Serial.println("\n--------------------------------------------------------");
    Serial.print("CACITOS: "); Serial.println(cacitos);
    leerLuz();
    leerTH();
    leerPresion();
    calcularRachaViento();
    esperaPoco=0;
    lluviaPing=0;

}
//------------------------------------------------------------------------------
void hacerAntesHora(){

  resetEsp();
  esperaCasiHora=0;

}
//------------------------------------------------------------------------------
void hacerCadaHora(){
    /* vuelca en el servidor y empieza otra vez */
    Serial.println("Hacer cada Hora");
    bool ok=false;
    while (!ok) ok=subir();
    espera1h=0;

}

/*************************************************
        L E C T U R A   S E N S O R E S
**************************************************/
void leerTH(){

    humedad = sensorTH.readHumidity();
    temp = sensorTH.readTemperature();
    Serial.print(" Temp:");
    Serial.println(temp, 2);
    Serial.print(" Humedad:");
    Serial.println(humedad, 2);
}
//------------------------------------------------------------------------------
void leerPresion(){

  presion = sensorPresion.readPressure();
  Serial.print(" Presion en pascales:");
  Serial.println(presion, 2);
}
//------------------------------------------------------------------------------
void leerLuz(){

      luz= get_light_level();
      Serial.print(" luz = ");
      Serial.print(luz);
      Serial.println("V");
}
//------------------------------------------------------------------------------
float get_light_level()
{
  float operatingVoltage = analogRead(REFERENCE_3V3);
  float lightSensor = analogRead(LIGHT);
  operatingVoltage = 3.3 / operatingVoltage; //The reference voltage is 3.3V
  lightSensor = operatingVoltage * lightSensor;
  return (lightSensor);
}

//------------------------------------------------------------------------------
void calcularRachaViento(){ //llamo cada POCO

    //En direccion de de la veleta almacena la direccion de la racha mayor !
    //un ping cada por sg = 2.4km/h (?)
    pingsViento+=pingsRacha;

    Serial.print("\n------> Racha Viento: ");
    float clicksSg=pingsRacha/(tRacha/1000.0);

    Serial.print("click/sg: "); Serial.println(clicksSg);
    Serial.print("pings Racha: "); Serial.println(pingsRacha);
    //rachaViento = max(clicksSg, rachaViento);

    if(clicksSg>rachaViento){
      rachaViento=clicksSg;
      direccionViento=leerVeleta();
    }
    Serial.print("racha Max: "); Serial.println(rachaViento);

    Serial.print("pings Hora: "); Serial.println(pingsViento);

    vientoMedio=pingsViento/(tViento/1000.0);
    Serial.print(" viento medio: "); Serial.println(vientoMedio);

    tRacha=0;
    pingsRacha=0;


}
//------------------------------------------------------------------------------
int leerVeleta()
{
    unsigned int adc;
    adc = analogRead(WDIR); // get the current reading from the sensor

    // The following table is ADC readings for the wind direction sensor output, sorted from low to high.
    // Each threshold is the midpoint between adjacent headings. The output is degrees for that ADC reading.
    // Note that these are not in compass degree order! See Weather Meters datasheet for more information.
    direccionViento=adc;


    if (adc < 380) return (113);
    if (adc < 393) return (68);
    if (adc < 414) return (90);
    if (adc < 456) return (158);
    if (adc < 508) return (135);
    if (adc < 551) return (203);
    if (adc < 615) return (180);
    if (adc < 680) return (23);
    if (adc < 746) return (45);
    if (adc < 801) return (248);
    if (adc < 833) return (225);
    if (adc < 878) return (338);
    if (adc < 913) return (0);
    if (adc < 940) return (293);
    if (adc < 967) return (315);
    if (adc < 990) return (270);




    return (-1); // error, disconnected?
}


/*************************************************
        W I F I
**************************************************/

void desconectarWifi(){

  /* si no me desconecto de la wifi termino por tumbarlo si hay mucho cacharros*/
  esp8266.println("desconectar()");
  Serial.println("desconectar la wifi");
  delay(1000);
  digitalWrite(ESP_ENABLE, LOW);

}
//------------------------------------------------------------------------------
bool subir(){

  /*

    Serial.println("*** S u b i r *****************");

    //QUE COJONES PASA CON LA lluvia que MARCA ??
    //if(cacitos != cacitosBis) {cacitos=cacitos*-1;}
    //'/aupa/<estacion>/<temp>/<hum>/<pres>/<luz>/<lluvia>/<velocidad>/<racha>/<direccion>/
    esp8266.println("isok()");
    if (leeEsp(2)) {

      Serial.println("Vamos pa´llá");
      //litros=cacitos*cacito*CORRIGE_PLU;
      String params=String(temp)+"/"+String(humedad)+"/"+String(presion)+"/"
          +String(luz)+"/"+String(cacitos)+"/"+String(vientoMedio)+"/"
          +String(rachaViento)+"/"+String(direccionViento)+"/";
      String comando="up('"+RECURSO+"','"+params+"')";

      Serial.println("Enviar al esp");
      Serial.println(comando);
      esp8266.println(comando);
      leeEsp(50);
      resetear();
    }else{

        Serial.println("No estoy conectado ");
        resetEsp();
        return false;
    }
    Serial.println("****** continuar ********");
    */
    return true;
}
//------------------------------------------------------------------------------
void resetear(){

      Serial.println("Reset");
      cacitos=0;

      litros=0;
      rachaViento=0;
      vientoMedio=0;
      pingsViento=0;
      tViento=0;
      desconectarWifi();

}
//------------------------------------------------------------------------------
bool leeEsp(int s){ //s segundos
  s=s*100;
  char c;
  int x=0;
  for(int i=0;i<s;i++){

    while(esp8266.available()>0){
      c=(char)esp8266.read();
      Serial.print(c);
      if (c=='!') return true;
    }
    delay(10);
    x++;
    if(x>100){Serial.println("_"); x=0;}

  }
  return false;
}
//------------------------------------------------------------------------------
void resetEsp(){

    Serial.println("despertar esp");
    digitalWrite(ESP_ENABLE, LOW);
    delay(1000);
    digitalWrite(ESP_ENABLE, HIGH);
    delay(1000);
    esp8266.println("");
    String c="servidor='"+IP+"'";
    esp8266.println(c);
    leeEsp(2);
    c="puerto="+PUERTO;
    esp8266.println(c);
    leeEsp(2);
    String con="\nW.connect('"+SSID+"', '"+PASSWORD+"')";
    esp8266.println(con);
    Serial.println(con);
    leeEsp(2);
    delay(8000);
    esp8266.println("isok()");
    leeEsp(2);
}


//******************************************************************************
// ISR ! INTERRUPT SERVICE ROUTINE.
// las variables deben ser volatile y las funciones los más breves posible
//******************************************************************************/


void rainIRQ(){

    if (cambio == 0) cacitos += 1;
    cambio=1;
}
// =============================================================================

void anemoIRQ(){

    if (anemoCambio == 0) anemoTics += 1;
    anemoCambio = 1;
}
