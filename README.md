# README #

## platformio

**Que es es?** Un pluggin para ATOM para programar microcontroladores
como arduino y otros

Es un IDE con muchas ventajas (autocompletar, instalación de paquetes,
colapsado de funciones ... etc), pero un poco **latoso** de instalar y poner
en marcha

### instalación

* instalar atom
* help > wellcome guide > install pack > open nstaller > 'platformio-ide'
* Si se queja: instalar clang
  * ubuntu: sudo apt-get install clang
  * restart
* Si todo ha ido bien se abrirá la home de platformio:
    * new project
    * elegir placa
    * quitar "default location"
    * elegir carpeta
    * Me crea una serie de ficheros
      * El codigo no está en .ino sin en .cpp
      * el resto es más o menos igual


      #include <Arduino.h>

      void setup() {
      // put your setup code here, to run once:
      Serial.begin(9600);
      Serial.println("Hola\n" );
      }
      void loop() {
          // put your main code here, to run repeatedly:
      }

  ### instalar una librería

  voy al home de platformio, librerías, la busco por nombre y me dejo llevar.

  #### Instalar

  * elapsedMillis
  * HTU21D: temperatura y humedad
  * MPL3115A2: presión
