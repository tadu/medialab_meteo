#include <Arduino.h>

//plu
const byte RAIN = 2;
volatile int cacitos = 0;
volatile int cambio=0;

//anemo
const byte ANEMO = 3;
volatile int anemoTics = 0;
volatile int anemoCambio=0;

void anemoIRQ();
void rainIRQ();
void setup() {


    //pluviometro
    pinMode(RAIN, INPUT_PULLUP); // input from wind meters rain gauge sensor
    attachInterrupt(digitalPinToInterrupt(RAIN), rainIRQ, FALLING);

    //anemometro
    pinMode(ANEMO, INPUT_PULLUP); // input from wind meters rain gauge sensor
    attachInterrupt(digitalPinToInterrupt(ANEMO), anemoIRQ, CHANGE);

    Serial.begin(9600);
    Serial.println("Hola\n" );

}

void loop() {

    // check pluviometro
    if (cambio==1){

        Serial.println(cacitos);
        cambio=0;
    }
    
    // check anemometro
    if (anemoCambio==1){
        Serial.print(".");
        anemoCambio=0;
    }
}

// =============================================================================

void rainIRQ(){

    if (cambio == 0) cacitos += 1;
    cambio=1;
}
// =============================================================================

void anemoIRQ(){

    if (anemoCambio == 0) anemoTics += 1;
    anemoCambio = 1;
}
